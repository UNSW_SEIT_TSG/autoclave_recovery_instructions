# Autoclave System Recovery Instructions
![N|Solid](title.png)
## UNSW Canberra School of Engineering and Information Technology

This documentation can help recover the Autoclave software in case of either:

* User Interface Loss due to unloading the uui [see here](#case-user-interface-problem).
* Hard drive failure. [see here](#case-hard-drive-failure).

### Case: User Interface Problem

This may be caused by the .uui file not being loaded correctly.

To remedy this one can either view the video [here][uui] or follow the written instructions below.

#### The cause -- Loading Recipe incorrectly
The following images show an attempt at loading a recipe via the open project command.  This causes the user interface to be lost until restored.
![N|Solid](Recipe_Problem/2019-03-07_UnivNSW_53.jpg)
![N|Solid](Recipe_Problem/2019-03-07_UnivNSW_54.jpg)
![N|Solid](Recipe_Problem/2019-03-07_UnivNSW_55.jpg)
![N|Solid](Recipe_Problem/2019-03-07_UnivNSW_56.jpg)

At this point the interface is lost and restarting the PC will not restore it.

To restore the interface return to open project.

![N|Solid](Recipe_Problem/2019-03-07_UnivNSW_58.jpg)

Open the ```Autoclave1.uui``` file in the directory: ```c:\American Autoclave\Autoclave1\Display\```

![N|Solid](Recipe_Problem/2019-03-07_UnivNSW_59.jpg)

This simple step restores the interface for normal use and persists after restarting the computer.

![N|Solid](Recipe_Problem/2019-03-07_UnivNSW_60.jpg)

### Case: Hard Drive Failure

The following text based instructions are supported by an instructional video [here][main].

#### [Removal of Old Hard Drive][link_00]
* Isolate power at socket and removing power cable from PC.
* [Open case using pull-latch-mechanism][link_01].
* [Remove door of case][link_02].
* [Unlatch hard drive power cable from cable tie][link_03].
* [Disconnect power and data cables from hard drive][link_04].
* [Squeeze caddy at light blue squeeze points to release from chassis][link_05].
* [Pull down on blue levers on side of caddy to remove the hard drive][link_06].

#### [Insertion of New Hard Drive][link_07]
* [Prepare blank drive for insertion][link_08].
* [Insert new drive into chassis by aligning screw cavities with metal studs of caddy, and pushing the blue levers on the side of the caddy until the disk is secured within the caddy][link_09].
* [Reconnect both data and power cables to hard drive][link_10].
* [Mate the lip of the caddy with the chassis and rotate until it clicks into place][link_11].
* [Stow the cable tie][link_12].
* [Replace the door of the case][link_13].

#### [Connect cables][link_14]
* [Note power supply setting matches the type of power to be supplied to the machine][link_15].
* [Connect power cable][link_16].
* [Connect network cable to AAC Support ethernet port][link_17]
* [Connect VGA port to screen][link_18]
* [Connect USB Keyboard & Mouse][link_19]

#### [Prepare backup files for access over network][link_20]
* [Access the files on a networked pc][link_21].
* [Share the folder containing the folder named 'WindowsImageBackup' over the network][link_22]
  * Right click on the parent folder and select properties
  * Click tab: 'Sharing'
  * Click button: 'Share...'
  * Ensure there is a user listed for sharing for which you have the password
    * Add your user if necessary by entering the user name and clicking 'Add'
  * Click button 'Share'
  * Click button 'Done'
  * Close Properties Dialog window by clicking button 'Close'
* [Note the IP Address of the networked pc containing the backup data so it can be accessed from the lab pc][link_23].
  * Hold the windows key '⊞ Win' then tap 'R' to open the 'Run' dialog box.
  * Type cmd.
  * Click button: 'Ok'
  * Wait for command prompt to open.
  * Type 'ipconfig'
  * Press Enter
  * Note the value 'IPv4 Address', the value should be something similar to: '131.236.54.148'.
* [Test Shared Directory][link_24]
  * Hold the windows key '⊞ Win' then tap 'R' to open the 'Run' dialog box.
  * type '\\'+{IPv4 Address}+'\'+{directory_containing_WindowsImageBackup} it should look like this: '\\131.236.54.148\autoclave_c_image'
  * Press Enter.
  * If the folder opens then the address is correct and the folder is shared.

#### [Restore backup to Lab PC][link_25]
* [Power on the PC][link_26]
* [Open CD tray and insert the Windows 32 bit Recovery Disc][link_27]
* [Boot from the disc (may require pressing F1 or a restart once the disc has been inserted)][link_28]
* [Follow System Recovery Prompts][link_29]
  * System Recovery Options>
    * Select 'US' Keyboard Input Method
    * Press button: 'Next'
    * Select 'Restore your computer using a system image that you created'
    * Press button: 'Next'
    * When prompted that 'Windows cannot find a system image on this computer' click button: 'Cancel'
    * Choose radio button 'Select a system image' and press button: 'Next'
    * Press button: 'Advanced'
    * Select 'Search for a system image on the network'
    * When prompted: 'Are you sure you want to connect to the network?', click button: 'Yes'
    * Wait for Lab pc to connect to the network.
    * [Enter the address of the recovery data][link_30].  This should look something like: '\\131.236.54.148\autoclave_c_image'.  It is the address we tested in the Test Shared Directory step earlier in this process.
    * Press button: 'OK'
    * [Enter username and password][link_31] for accessing the shared network location.
    * Press button: 'OK'
    * The disc image should now be listed.  Select it and then press 'Next >'
    * Select it again and then press 'Next >'
    * Press button: 'Next >'
    * Press button: 'Finish'
    * When prompted 'All disks to be restored will be formatted and replaced with the layout and data in the system image.  Are you sure you want to continue?', press button: 'Yes'
    * Wait for Re-image process to complete.
* [Test that system boots correctly][link_32]
  * Remove Recovery CD from drive and boot to windows normally.

   [main]: <https://youtu.be/ULSi3WeWFWA>
   [link_00]: <https://youtu.be/ULSi3WeWFWA?t=69>
   [link_01]: <https://youtu.be/ULSi3WeWFWA?t=87>
   [link_02]: <https://youtu.be/ULSi3WeWFWA?t=96>
   [link_03]: <https://youtu.be/ULSi3WeWFWA?t=99>
   [link_04]: <https://youtu.be/ULSi3WeWFWA?t=102>
   [link_05]: <https://youtu.be/ULSi3WeWFWA?t=106>
   [link_06]: <https://youtu.be/ULSi3WeWFWA?t=110>
   [link_07]: <https://youtu.be/ULSi3WeWFWA?t=126>
   [link_08]: <https://youtu.be/ULSi3WeWFWA?t=130>
   [link_09]: <https://youtu.be/ULSi3WeWFWA?t=150>
   [link_10]: <https://youtu.be/ULSi3WeWFWA?t=163>
   [link_11]: <https://youtu.be/ULSi3WeWFWA?t=174>
   [link_12]: <https://youtu.be/ULSi3WeWFWA?t=178>
   [link_13]: <https://youtu.be/ULSi3WeWFWA?t=182>
   [link_14]: <https://youtu.be/ULSi3WeWFWA?t=188>
   [link_15]: <https://youtu.be/ULSi3WeWFWA?t=194>
   [link_16]: <https://youtu.be/ULSi3WeWFWA?t=206>
   [link_17]: <https://youtu.be/ULSi3WeWFWA?t=210>
   [link_18]: <https://youtu.be/ULSi3WeWFWA?t=217>
   [link_19]: <https://youtu.be/ULSi3WeWFWA?t=225>
   [link_20]: <https://youtu.be/ULSi3WeWFWA?t=240>
   [link_21]: <https://youtu.be/ULSi3WeWFWA?t=260>
   [link_22]: <https://youtu.be/ULSi3WeWFWA?t=271>
   [link_23]: <https://youtu.be/ULSi3WeWFWA?t=303>
   [link_24]: <https://youtu.be/ULSi3WeWFWA?t=341>
   [link_25]: <https://youtu.be/ULSi3WeWFWA?t=376>
   [link_26]: <https://youtu.be/ULSi3WeWFWA?t=376>
   [link_27]: <https://youtu.be/ULSi3WeWFWA?t=381>
   [link_28]: <https://youtu.be/ULSi3WeWFWA?t=395>
   [link_29]: <https://youtu.be/ULSi3WeWFWA?t=443>
   [link_30]: <https://youtu.be/ULSi3WeWFWA?t=470>
   [link_31]: <https://youtu.be/ULSi3WeWFWA?t=497>
   [link_32]: <https://youtu.be/ULSi3WeWFWA?t=550>
   [uui]: https://youtu.be/1qW3wgaCJgs